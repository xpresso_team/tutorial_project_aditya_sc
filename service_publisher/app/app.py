"""
This is a service-publisher hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Naveen Sinha"

import json
from json import JSONDecodeError
import logging
from flask import Flask
from flask import request

config_file = 'config/dev.json'


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    return flask_app


app = create_app()


@app.route('/')
def hello_world():
    """
    Send response to Hello World
    """
    logger = logging.getLogger()
    logger.info("Received request from {}".format(request.remote_addr))
    try:
        logger.info("Processing the request")
        cfg_fs = open(config_file, 'r')
        config = json.load(cfg_fs)
        project_name = config["project_name"]
        logger.info("Request Processing Done")
        logger.info("Sending Response to {}".format(request.remote_addr))
        return '<html><body><b>Hello World from {}!</b></body></html>'.format(
            project_name
        )
    except (FileNotFoundError, JSONDecodeError):
        logger.error("Request Processing Failed")
        logger.info("Sending Default Response")
        return '<html><body><b>Hello World!</b></body></html>'


if __name__ == '__main__':
    app.run()
